Program GetInt (input, Output);

uses Crt, SysUtils;

var
k,c,a:string;
p  :String = 'Hello';
ch:char;
num:integer;

Begin
{*p:='Enter an integer number: ';*}
WriteLn(p);
SetLength(k,0);
Repeat
    k[Length(k)+1]:=ReadKey;
    write(k[Length(k)+1]);
    if (k[Length(k)+1] = #45) and (length(k) = 0) then Inc(k[0]);
    if (k[Length(k)+1] > #45) and (k[Length(k)+1] < #56) then Inc(k[0]);
until (k[Length(k)+1] = #13) or (length(k) = 10);
Writeln('k is ',k);

a := '1';
c := '2';
k := Concat(a,c);
Writeln('k is ',k);

end.