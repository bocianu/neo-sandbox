program lisajous;
uses neo6502,neo6502math;
const 
    BCOUNT = 90;
    BDIST = 4;
var tx,ty,ti:float;
    xy:float;
    b:byte;
    bx:array[0..BCOUNT-1] of word;
    by:array[0..BCOUNT-1] of word;

begin
    tx:=0;
    ty:=0;
    NeoResetPalette;
    NeoSetSolidFlag(1);
    SetDegreeMode;
    xy:=0;
    repeat
        ti := 0;
        for b:=0 to BCOUNT-1 do begin
            SetMathVar(tx + ti);
            DoMathOnVar(MATHSin);
            SetMathStack(m_float,0);
            SetMathStack(float(150),1);
            DoMathOnStack(MATHMul);
            bx[b] := Round(GetMathStackFloat) + 155;
            SetMathVar(ty + ti);
            DoMathOnVar(MATHCos);
            SetMathStack(m_float,0);
            SetMathStack(float(110),1);
            DoMathOnStack(MATHMul);
            by[b] := Round(GetMathStackFloat) + 115;
            ti := ti + BDIST;
        end;

        NeoWaitForVblank;
        NeoSetColor(0);
        NeoDrawRect(0,0,319,239);
        for b:=0 to BCOUNT-1 do begin
            NeoSetColor(b and 15);
            NeoDrawEllipse(bx[b],by[b],bx[b]+9,by[b]+9);
        end;
        tx := tx+1;
        ty := ty+1.2;
        //xy:=xy+3;
    until false;


end.