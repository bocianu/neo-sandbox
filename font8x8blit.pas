
uses neo6502,crt;

const 
    FONT = $7000;
    C8_SCR_WIDTH = 40;
    C8_SCR_HEIGHT = 30;
    C8_SCR_SIZE = C8_SCR_WIDTH * C8_SCR_HEIGHT;

{$r res.rc}

var c8_vram:array [0..C8_SCR_SIZE-1] of char;
    blockGFX,blockScr: TblitBlock;
    c8_lmargin:byte;
    c8_rmargin:byte;
    c8_cx:byte;
    c8_cy:byte;
    c8_off:word;
    c8_offsets: array of word = [ {$EVAL C8_SCR_HEIGHT,"8 * 320 * :1"} ];

procedure Blit;
begin
    repeat until not NeoBlitterBusy;
    NeoBlitterXCopy(BLTACT_SOLID,@blockGFX,@blockScr);
end;

procedure C8_UpdateCOff;
begin
    c8_off:=c8_offsets[c8_cy]+(c8_cx shl 3);
end;

procedure C8_Forward;
begin
    Inc(c8_cx);
    if (c8_cx=c8_rmargin) or (c8_cx=C8_SCR_WIDTH) then begin
        c8_cx := c8_lmargin;
        if c8_cx<C8_SCR_HEIGHT-1 then Inc(c8_cy);
    end;
    C8_UpdateCOff;
end;

procedure C8_GotoXY(x,y:byte);
begin
    c8_cx:=x;
    c8_cy:=y;
    C8_UpdateCOff;
end;

procedure C8_LineFeed;
begin
    Inc(c8_cy);
    c8_cx:=c8_lmargin;
    C8_UpdateCOff;
end;

procedure C8_DramVram;
var off:word;
    x:byte;
begin
    x := 0;
    off := 0;
    blockScr.address := MEM_VRAM;
    while off < C8_SCR_HEIGHT * C8_SCR_WIDTH do begin
        blockGFX.address := FONT + (byte(c8_vram[off]) shl 3);
        Blit;
        Inc(off);
        Inc(x);
        Inc(blockScr.address, 8);
        if x = C8_SCR_WIDTH then begin
            x := 0;
            Inc(blockScr.address, 320 * 7);
        end;
    end;
end;

function A2I(c: byte): byte;
begin
    asm {
        lda c
        asl
        php
        cmp #2*$60
        bcs @+
        sbc #2*$20-1
        bcs @+
        adc #2*$60
@       plp
        ror
        sta result;
    };
end;

procedure C8_PutCursor;
var x,y:byte;
begin
    x := c8_cx shl 3;
    y := c8_cy shl 3;
    NeoSetColor($ff);
    NeoSetSolidFlag(1);
    NeoDrawRect(x, y, x + 7, y + 7);
end;

procedure C8_Init;
begin
    blockGFX.width := 8;
    blockGFX.stride := 1;
    blockGFX.height := 8;
    blockGFX.format := BLTFORM_1BPP;
    blockGFX.solid := $ff;
    blockGFX.transparent := 0;

    blockScr.width := 8;
    blockScr.stride := 320;
    blockScr.height := 8;
    blockScr.format := 0;

    c8_lmargin:=2;
    c8_rmargin:=40;
    c8_cx:=0;
    c8_cy:=0;
    C8_LineFeed;
end;

procedure C8_PutChar(ch:char);
begin
    blockGFX.address := FONT + (A2I(byte(ch)) shl 3);
    blockScr.address := MEM_VRAM + c8_off;
    Blit;
    C8_Forward;
end;

procedure C8_Write(s:string);
var i:byte;
begin
    for i:=1 to byte(s[0]) do C8_PutChar(s[i]);
end;

var c: byte;
    t: cardinal;

begin
    C8_Init;
    NeoSetPalette(0,$10,$50,$70);
    NeoSetPalette($ff,$60,$d0,$f0);
    ClrScr;
    NeoCursorInverse;
    C8_Write('READY');
    C8_LineFeed;
    C8_LineFeed;
    for c := 0 to 127 do C8_PutChar(char(c));
    C8_LineFeed;
    C8_PutCursor;

    readkey;

    repeat
        NeoWaitForVblank;
        t:=NeoGetVblanks;
        C8_DramVram;
        t:=NeoGetVblanks-t;
        Write(t);
        //readkey;
    until false;
    
    readkey;
end.
