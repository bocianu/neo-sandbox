uses crt,neo6502,neo6502math;
const n = 200;
    BUFFER=$8000;
    CHUNKSIZE=320*80;
    IMGSIZE=320*240;
var 
    g:float absolute($80);
    r:float absolute($84);
    x:float absolute($88);
    y:float absolute($8c);
    v:float absolute($90);
    t:float absolute($94);
    u:float absolute($98);
    p:float absolute($9c);
    q:float absolute($a0);
    xx:float absolute($a4);
    yy:float absolute($a8);
    
    c,i,j,s:byte;
    fname:TString;
    frame:word;

procedure DrawFrame;
begin
    NeoSetColor($ff);
    NeoDrawRect(0,0,319,239);
    for i:=0 to n do 
        for j:=0 to n do begin
            SetMathStack(i,0);
            SetMathStack(v,1);
            DoMathOnStack(MATHAdd);
            SetMathStack(g,1);
            DoMathOnStack(MATHMul);
            p := GetMathStackFloat;

            SetMathStack(i,0);
            SetMathStack(r,1);
            DoMathOnStack(MATHMul);
            SetMathStack(x,1);
            DoMathOnStack(MATHAdd);
            SetMathStack(g,1);
            DoMathOnStack(MATHMul);
            q := GetMathStackFloat; 
            
            SetMathVar(p);
            DoMathOnVar(MATHSin);
            SetMathStack(m_float,0);
            SetMathVar(q);
            DoMathOnVar(MATHSin);
            SetMathStack(m_float,1);
            DoMathOnStack(MATHAdd);
            u:=GetMathStackFloat;
            SetMathStack(s,1);
            DoMathOnStack(MATHMul);
            xx:=GetMathStackFloat;

            SetMathVar(p);
            DoMathOnVar(MATHCos);
            SetMathStack(m_float,0);
            SetMathVar(q);
            DoMathOnVar(MATHCos);
            SetMathStack(m_float,1);
            DoMathOnStack(MATHAdd);
            v:=GetMathStackFloat;
            SetMathStack(s,1);
            DoMathOnStack(MATHMul);
            yy:=GetMathStackFloat;
           
            x:=u+t;
            NeoSetColor(((i div 14) shl 4) or (j div 14));
            NeoDrawPixel(Trunc(xx)+160,Trunc(yy)+120);
        end;
end;

procedure SaveFrame;
var
  vram:cardinal;
  wlen,len:integer;
begin
    vram := MEM_VRAM;
    len := IMGSIZE;
    NeoStr(frame,fname);
    fname := Concat(fname,'.img');
    fname := Concat('f_',fname);
    NeoOpenFile(0,@fname[0],OPEN_MODE_NEW);
    while len>0  do begin
        NeoBlitterCopy(vram,BUFFER,CHUNKSIZE);
        wlen := NeoWriteFile(0,BUFFER,CHUNKSIZE);
        inc(vram,wlen);
        dec(len,wlen);
    end;
    NeoCloseFile(0);
end;

begin
    // prepare palette
    NeoSetDefaults(0,$ff,1,1,0);
    NeoSetPalette($ff,0,0,0);
    NeoSetColor($ff);
    NeoDrawRect(0,0,319,239);

    for i:=0 to n do 
        for j:=0 to n do begin
            c:=((i div 14) shl 4) or (j div 14);
            NeoSetPalette(c, i, j, 99);
        end;

    x:=0; y:=0; v:=0; t:=0; s:=60;
    r := (PI * 2) / 235;
    g := 360 / (PI *2);
    frame := 0;

    repeat
        DrawFrame;
        SaveFrame;
        Inc(frame);
        t:=t+0.025;
    until false;

end.
