uses crt,neo6502,neo6502Math;
const n = 200;
var r,x,y,v,t,u,p,q:single;
    i,j,s:byte;
    time:cardinal;
    tmpstr:TString;
begin
    // prepare palette
    NeoSetDefaults(0,$ff,1,1,0);
    NeoSetPalette($ff,0,0,0);
    for i:=0 to 14 do 
        for j:=0 to 14 do 
            NeoSetPalette((i shl 4) or j,i*18,j*18,100);
    // draw
    x:=0; y:=0; v:=0; t:=0; s:=60;
    r:=(PI * 2)/235;
    time:=NeoGetTimer;
    //repeat
        NeoSetColor($ff);
        NeoDrawRect(0,0,319,239);
        for i:=0 to n do 
            for j:=0 to n do begin
                p:=i+v;
                q:=(r*i)+x;
                u:=sin(p)+sin(q);
                v:=cos(p)+cos(q);
                x:=u+t;
                NeoSetColor(((i div 14) shl 4) or (j div 14));
                NeoDrawPixel(Round(u*s)+160,Round(v*s)+120);
            end;
        t:=t+0.025;
    time:=(NeoGetTimer - time) div 100;
    //until false;
    NeoSetSolidFlag(0);
    NeoSetColor($ee);
    Gotoxy(1,1);
    NeoStr(time,tmpstr);
    NeoDrawString(0,0,tmpstr);
    repeat until keypressed;
end.
