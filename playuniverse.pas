uses crt,neo6502,neo6502math;
const n = 200;
    BUFFER=$8000;
    CHUNKSIZE=320*80;
    IMGSIZE=320*240;
var 
    c,i,j,s:byte;
    fname:TString;
    frame:word;


procedure LoadFrame;
var
  vram:cardinal;
  wlen,len:integer;
begin
    vram := MEM_VRAM;
    len := IMGSIZE;
    NeoStr(frame,fname);
    fname := Concat(fname,'.img');
    fname := Concat('f_',fname);
    if NeoOpenFile(0,@fname[0],OPEN_MODE_RO) then 
    while len>0  do begin
        wlen := NeoReadFile(0,BUFFER,CHUNKSIZE);
        NeoBlitterCopy(BUFFER,vram,wlen);
        inc(vram,wlen);
        dec(len,wlen);
    end else frame :=0;
    NeoCloseFile(0);
end;

begin
    // prepare palette
    NeoSetDefaults(0,$ff,1,1,0);
    NeoSetPalette($ff,0,0,0);
    NeoSetColor($ff);
    NeoDrawRect(0,0,319,239);

    for i:=0 to n do 
        for j:=0 to n do begin
            c:=((i div 14) shl 4) or (j div 14);
            NeoSetPalette(c, i, j, 99);
        end;

    frame := 0;

    repeat
        NeoWaitForVblank;
        LoadFrame;
        Inc(frame);
    until false;

end.
