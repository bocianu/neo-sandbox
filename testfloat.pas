program test;
uses crt;

var f:float;

begin
    f:=100;
    if f > 0.1 then Writeln('1');
    if 0.1 < f then Writeln('2');
    if f > float(0.1) then Writeln('3');
    if float(0.1) < f then Writeln('4');
    if f > 1 then Writeln('5');
    if 1 < f then Writeln('6');
    if f < 1000 then Writeln('7');
    if 1000 > f then Writeln('8');
end.