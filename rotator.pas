program rotator;
uses crt,neo6502,neo6502math;
const 
    POINTS_NUM = 100;

var 
    rotAxisX,rotAxisY:word;
    p_x:array[0..99] of float = (
        -80,-70,-40,-20,-10,0,10,20,50,60,70,-80,-70,-60,-40,-20,-10,40,50,80,-80,-70,-60,-50,-40,-20,-10,0,10,20,40,50,80,-80,-70,-50,-40,-20,-10,40,50,80,-80,-70,-40,-20,-10,40,50,80,-80,-70,-40,-20,-10,0,10,20,50,60,70,-70,-60,-30,-20,-10,20,30,60,70,-80,-30,10,40,80,-80,-70,-60,-30,-20,-10,10,40,70,80,-80,-50,-10,10,40,60,-70,-60,-30,-20,20,30,60,70,80
    );
    p_y:array[0..99] of float = (
        -55,-55,-55,-55,-55,-55,-55,-55,-55,-55,-55,-45,-45,-45,-45,-45,-45,-45,-45,-45,-35,-35,-35,-35,-35,-35,-35,-35,-35,-35,-35,-35,-35,-25,-25,-25,-25,-25,-25,-25,-25,-25,-15,-15,-15,-15,-15,-15,-15,-15,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,15,15,15,15,15,15,15,15,15,25,25,25,25,25,35,35,35,35,35,35,35,35,35,35,45,45,45,45,45,45,55,55,55,55,55,55,55,55,55
    );
    spinSpeed:smallInt;
    px,py,w:word;
    b,c:byte;
    x,y,tcos,tsin:float;
    
    spin_sequence: array of SmallInt = [ 
        8,32, 2,24, 2,18, 3,12, 4,8, 4,4, 8,3, 10,2, 12,1, 2,0, 
        12,-1, 10,-2, 8,-3, 4,-4, 4,-8, 3,-12, 2,-18, 2,-24, 
        8,-32, 2,-24, 2,-18, 3,-12, 4,-8, 4,-4, 8,-3, 10,-2, 12,-1, 2,0, 
        12,1, 10,2, 8,3, 4,4, 4,8, 3,12, 2,18, 2,24
    ];
    spin_counter: byte;
    spin_step: byte;

procedure RecalcAngle;inline;
begin
    SetMathVar(spinSpeed);
    DoMathOnVar(MATHSin);
    tsin := m_float;
    SetMathVar(spinSpeed);
    DoMathOnVar(MATHCos);
    tcos := m_float;
end;

procedure RecalcPoint;inline;
begin
    SetMathStack(p_x[w],0);
    SetMathStack(tcos,1);
    DoMathOnStack(MATHMul);
    x:=GetMathStackFloat;
    SetMathStack(p_y[w],0);
    SetMathStack(tsin,1);
    DoMathOnStack(MATHMul);
    x:=x-GetMathStackFloat;
    SetMathStack(p_y[w],0);
    SetMathStack(tcos,1);
    DoMathOnStack(MATHMul);
    y:=GetMathStackFloat;
    SetMathStack(p_x[w],0);
    SetMathStack(tsin,1);
    DoMathOnStack(MATHMul);
    y:=y+GetMathStackFloat;
    p_x[w]:=x;
    p_y[w]:=y;
end;

procedure ReadNextSpinSpeed;
begin
    spin_counter := spin_sequence[spin_step] shl 1;
    spinSpeed := spin_sequence[spin_step+1];
    spin_step:= spin_step + 2;
    if spin_step >= (SizeOf(spin_sequence) shr 1) then spin_step:=0;
    RecalcAngle;
end;

procedure FadeTo(c,r,g,b:byte);
var cr,cg,cb:byte;
begin
    NeoGetPalette(c,cr,cg,cb);
    if r<cr then cr:=cr-$10;
    if g<cg then cg:=cg-$10;
    if b<cb then cb:=cb-$10;
    if r>cr then cr:=cr+$10;
    if g>cg then cg:=cg+$10;
    if b>cb then cb:=cb+$10;
    NeoSetPalette(c,cr and $f0,cg and $f0,cb and $f0);
end;
            
begin
    // init;
    rotAxisX:=160;
    rotAxisY:=120;
    SetDegreeMode;
    spin_step := 0;
    ReadNextspinSpeed;

    // clear below logos
    NeoSetColor(0);
    NeoSetSolidFlag(1);
    NeoDrawRect(0,150,320,240);

    // move logo down
    w:=0;
    for b:=0 to 40 do begin
        for c:=0 to b shr 3 do NeoWaitForVblank;
        repeat until not NeoBlitterBusy;
        NeoBlitterCopy(MEM_VRAM+w,MEM_VRAM+w+320,320*140);
        w:=w+320;
    end;

    // fadeout logo
    for w:=0 to 16 do begin
        for b:=0 to 5 do NeoWaitForVblank;
        FadeTo(6,$10,$30,$40);
        FadeTo(5,$40,$00,$50);
        FadeTo(1,$40,$10,$10);
    end;    

    // main loop    
    repeat 
        if spin_counter > 0 then begin
            dec(spin_counter);
            if spin_counter = 0 then ReadNextSpinSpeed;
        end;
    
        for w:=0 to POINTS_NUM-1 do begin
            RecalcPoint;
            px := Trunc(x) + rotAxisX;
            py := Trunc(y) + rotAxisY;
            NeoUpdateSprite(w,px,py,0,0,0);
        end; 
    until false;
end.