uses crt, neo6502;

var b:byte;

procedure NeoGetPalette(col:byte);
begin
    NeoMessage.params[0]:=col;
    NeoSendMessage(5,38);
end;

begin

    NeoResetPalette;
    ClrScr;
     for b:=0 to 15 do begin
        NeoGetPalette(b);
        Write('Color: ',b);
        Write('   R: ',NeoMessage.params[1]);
        Write('   G: ',NeoMessage.params[2]);
        Write('   B: ',NeoMessage.params[3]); 
        Writeln;
        NeoMessage.params[0]:=b;
        NeoSendMessage(5,32);
    end;

    ReadKey;

end.